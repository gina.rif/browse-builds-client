export const europeanCountries = [
    {
        id: "AX",
        phone: "+358"
    },
    {
        id: "AL",
        phone: "+355"
    },
    {
        id: "AD",
        phone: "+376"
    },
    {
        id: "AT",
        phone: "+43"
    },
    {
        id: "BY",
        phone: "+375"
    },
    {
        id: "BE",
        phone: "+32"
    },
    {
        id: "BA",
        phone: "+378"
    },
    {
        id: "BG",
        phone: "+359"
    },
    {
        id: "HR",
        phone: "+385"
    },
    {
        id: "CZ",
        phone: "+420"
    },
    {
        id: "DK",
        phone: "+45"
    },
    {
        id: "EE",
        phone: "+372"
    },
    {
        id: "FO",
        phone: "+298"
    },
    {
        id: "FI",
        phone: "+358"
    },
    {
        id: "FR",
        phone: "+33"
    },
    {
        id: "DE",
        phone: "+49"
    },
    {
        id: "GI",
        phone: "+350"
    },
    {
        id: "GR",
        phone: "+30"
    },
    {
        id: "VA",
        phone: "+39"
    },
    {
        id: "HU",
        phone: "+36"
    },
    {
        id: "IS",
        phone: "+354"
    },
    {
        id: "IE",
        phone: "+353"
    },
    {
        id: "IM",
        phone: "+44"
    },
    {
        id: "IT",
        phone: "+39"
    },
    {
        id: "JE",
        phone: "+44"
    },
    {
        id: "XK",
        phone: "+381"
    },
    {
        id: "LV",
        phone: "+371"
    },
    {
        id: "LI",
        phone: "+423"
    },
    {
        id: "LT",
        phone: "+370"
    },
    {
        id: "LU",
        phone: "+352"
    },
    {
        id: "MK",
        phone: "+389"
    },
    {
        id: "MT",
        phone: "+356"
    },
    {
        id: "MD",
        phone: "+373"
    },
    {
        id: "MC",
        phone: "+377"
    },
    {
        id: "ME",
        phone: "+382"
    },
    {
        id: "NL",
        phone: "+31"
    },
    {
        id: "NO",
        phone: "+47"
    },
    {
        id: "PL",
        phone: "+48"
    },
    {
        id: "PT",
        phone: "+351"
    },
    {
        id: "RO",
        phone: "+40"
    },
    {
        id: "SM",
        phone: "+378"
    },
    {
        id: "RS",
        phone: "+381"
    },
    {
        id: "CS",
        phone: "+381"
    },
    {
        id: "SK",
        phone: "+421"
    },
    {
        id: "SI",
        phone: "+386"
    },
    {
        id: "ES",
        phone: "+34"
    },
    {
        id: "SJ",
        phone: "+47"
    },
    {
        id: "SE",
        phone: "+46"
    },
    {
        id: "CH",
        phone: "+41"
    },
    {
        id: "UA",
        phone: "+380"
    },
    {
        id: "GB",
        phone: "+44"
    }
];