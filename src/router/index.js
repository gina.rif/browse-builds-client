import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "../stores/user";
// import BeforeRouteEnterCompany from './beforeRouteEnterCompany.js';
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/landing",
      name: "landing",
      component: () => import("../views/LandingView.vue"),
    },
    {
      path: "/",
      name: "feed",
      props: (route) => ({
        page: Number(route.query.page) || 1,
        view: Number(route.query.view) || 30,
        filter: {
          dateInterval: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).dateInterval ? JSON.parse(decodeURIComponent(route.query.filter)).dateInterval : { from: "", to: "" },
          categories: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).categories ? JSON.parse(decodeURIComponent(route.query.filter)).categories : [],
          companies: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).companies ? JSON.parse(decodeURIComponent(route.query.filter)).companies : [],
          addresses: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).addresses ? JSON.parse(decodeURIComponent(route.query.filter)).addresses : [],
          ratings: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).ratings ? JSON.parse(decodeURIComponent(route.query.filter)).ratings : [],
        },
        sort: route.query.sort || '_DATE_POSTED',
      }),
      component: () => import("../views/FeedView.vue"),
    },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import("../views/AboutView.vue"),
    // },
    {
      path: '/login',
      name: 'login',
      component: () => import("../views/LoginView.vue"),
      beforeEnter: (to, from, next) => {
        const userStore = useUserStore();
        if (userStore && userStore.authenticated) {
          next('/profile/'+userStore.currentUser.id);
        } else {
          next();
        }
      },
    },
    {
      path: '/oauth2/redirect',
      name: 'oauth2-redirect',
      props: (route) => ({
         token: route.query.token || "",
        error: route.query.error ||""}),
      // query: { token: "" },
      component: () => import("../views/OAuth2RedirectView.vue"),
      beforeEnter: (to, from, next) => {
       console.log("preparing to enter OAuth2RedirectView")
        next();
      },
    },
    {
      path: '/profile/:id(\\d+)',
      name: 'profile',
      props: (route) => ({
        id: Number(route.params.id) 
      }),
      component: () => import("../views/ProfileView.vue"),
    },
    {
      path: '/signin',
      name: 'signin',
      component: () => import("../views/SigninView.vue"),
      beforeEnter: (to, from, next) => {
        const userStore = useUserStore();
        if (userStore && userStore.authenticated) {
          next('/profile/'+userStore.currentUser.id);
        } else {
          next();
        }
      },
    },
    {
      path: '/activate',
      name: 'activate',
      props: (route) => ({
        invitationToken: route.query.invitationToken || "",
      }),
      // props: { invitationToken: "" },
      // query: { invitationToken: "" },
      component: () => import("../views/ActivateAccountView.vue"),
      beforeEnter: (to, from, next) => {
        const userStore = useUserStore();
        if (userStore && userStore.authenticated) {
          userStore.logout();
        }
        next();
      },
    },
    {
      path: '/newpost',
      name: 'newpost',
      component: () => import("../views/NewPostView.vue"),
      beforeEnter: (to, from, next) => {
        const userStore = useUserStore();
        if ( userStore.authenticated && userStore.currentUser.userType == "COMPANY") {
          next();
        } else {
          next('/unauthorized');
        }
      },
    },
    {
      path: '/post/:id(\\d+)',
      name: 'post',
      component: () => import("../views/PostView.vue"),
      props: (route) => ({
        id: Number(route.params.id) 
      }),
    },
    {
      path: '/not-found',
      name: 'not-found',
      component: () => import("../views/ErrorNotFoundView.vue"),
    },
    {
      path: '/unauthorized',
      name: 'unauthorized',
      component: () => import("../views/ErrorUnauthorizedView.vue"),
    },
    {
      path: '/companies',
      name: 'companies',
      props: (route) => ({
        page: Number(route.query.page) || 1,
        view: Number(route.query.view) || 30,
        filter: {
          categories: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).categories ? JSON.parse(decodeURIComponent(route.query.filter)).categories : [],
          addresses: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).addresses ? JSON.parse(decodeURIComponent(route.query.filter)).addresses : [],
          ratings: route.query.filter && JSON.parse(decodeURIComponent(route.query.filter)).ratings ? JSON.parse(decodeURIComponent(route.query.filter)).ratings : [],
        },
        sort: route.query.sort || 'ALPHABETICAL',
      }),
      component: () => import("../views/CompaniesView.vue"),
    },
    {
      path: '/privacy-policy',
      name: 'privacy-policy',
      component: () => import("../views/PrivacyPolicyView.vue"),
    },
    {
      path: '/terms-of-service',
      name: 'terms-of-service',
      component: () => import("../views/TermsOfServiceView.vue"),
    },
  ],
});

export default router;
