export function formatDate(datePosted) {
    if (datePosted == null) {
        return "- data necunoscută -";
    } else {
        const date = new Date(datePosted);
        const options = { day: 'numeric', month: 'long', year: 'numeric' };

        const start = Date.now();
        let secondsElapsed = (start - date.getTime()) / 1000;
        if (secondsElapsed < 1)
            return 'Chiar acum';
        if (secondsElapsed < 60)
            return 'Acum ' + parseInt(secondsElapsed) + ' secunde';
        if (secondsElapsed < 3600) {
            secondsElapsed = secondsElapsed / 60;
            return 'Acum ' + parseInt(secondsElapsed) + ' minute';
        }
        if (secondsElapsed < 86400) {
            secondsElapsed = secondsElapsed / 3600;
            return 'Acum ' + parseInt(secondsElapsed) + ' ore';
        }
        return new Intl.DateTimeFormat('ro-RO', options).format(date);
    }
}