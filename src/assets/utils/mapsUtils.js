import { GOOGLE_MAP_API_KEY as GOOGLE_MAP_API_KEY } from "../../constants";
  export async function geocodeAddress(county, city, street, number) {
    try {
        const resp = await fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" +
            number + " " + street + " " + city + " " + county + " Romania" +
            "&key=" + GOOGLE_MAP_API_KEY, {
            method: "GET",
        }).then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            }));
        const result = resp.results[0];
        const location = result.geometry.location;
        return location;
    } catch (error) {
        console.error('Geocoding failed:', error);
        return {lat:0,lng:0};
    }
};