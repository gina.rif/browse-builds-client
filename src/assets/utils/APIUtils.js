import { API_BASE_URL as API_BASE_URL } from "../../constants/index.js";
import { ACCESS_TOKEN as ACCESS_TOKEN } from "../../constants/index.js";

const request = async (options) => {
  const headers = new Headers({ "Content-Type": "application/json" });

  if (localStorage.getItem(ACCESS_TOKEN)) {
    // console.log("Access token set: ", localStorage.getItem(ACCESS_TOKEN));
    headers.append("Authorization", "Bearer " + localStorage.getItem(ACCESS_TOKEN));
  }
  if (options.isFormData) {
    headers.delete("Content-Type", "application/json");
  }
  const defaults = { headers: headers };
  options = Object.assign({}, defaults, options);

  return await fetch(options.url, options).then(response =>
    response.json().then(json => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    })
  );
};

// ----------------------------------------------------------User
export async function getCurrentUser() {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
     console.log("No access token set for getting the current user.");
    return Promise.reject("No access token set for getting the current user.");
  }
  console.log("Access token set for getting the current user.");
  return await request({
    url: API_BASE_URL + "/users/profile",
    method: "GET"
  });
}

export async function getUserById(id) {
  return await request({
    url: API_BASE_URL + "/users/" + id,
    method: "GET"
  });
}

export async function deleteCurrentUser() {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    console.log("No access token set for deletting the current user.");
   return Promise.reject("No access token set for deletting the current user.");
 }
  return await request({
    url: API_BASE_URL + "/users",
    method: "DELETE"
  });
}

export function getProviderFromEmail(email) {
    const providerRegex = /@([\w-]+)\./;
    const match = email.match(providerRegex);
    if (match && match.length > 1) {
      if(match.includes("gmail"))
        return "google";
      return match[1];
    }
    return null;
  }
// ----------------------------------------------------------Company
export async function postCompany(company) {
  var newCompanyRequest = {
    ...company,
    provider: getProviderFromEmail(company.email)
  };
  // return await request({
  //   url: API_BASE_URL + "/companies",
  //   method: "POST",
  //   body: JSON.stringify(newCompanyRequest)
  // }).catch(error => console.log(error));
  const headers = new Headers({ "Content-Type": "application/json" });

  return await fetch(API_BASE_URL + "/companies",  {
    method: "POST",
    headers: headers,
    body: JSON.stringify(newCompanyRequest)
  });
}

export async function putCompany(company, categoryIds, profilePicture) {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    // console.log("putCompany No access token set for getting the current user.");
    return Promise.reject("No access token set for getting the current user.");
  }
  const formData = new FormData();
  formData.append('id', company.id);
  formData.append('companyName', company.user.companyName);
  formData.append('description', company.user.description);
  formData.append('phone', company.user.phone);
  formData.append('taxId', company.user.taxId);
  formData.append('registrationNumber', company.user.registrationNumber);
  categoryIds.forEach((categoryId, index) => {
    formData.append(`categoryIds[${index}]`, categoryId);
  });
  formData.append('countryCode', company.user.address.countryCode);
  formData.append('county', company.user.address.county);
  formData.append('city', company.user.address.city);
  formData.append('street', company.user.address.street);
  formData.append('number', company.user.address.number);
  formData.append('latitude', company.user.address.latitude);
  formData.append('longitude', company.user.address.longitude);
  if (profilePicture) {
    formData.append(`profilePicture`, profilePicture);
  }
  return await request({
    url: API_BASE_URL + "/companies",
    isFormData: true,
    method: "PUT",
    body: formData
  });
}

export async function getAllCompaniesV1() {
  return await request({
    url: API_BASE_URL + "/companies/v1",
    method: "GET"
  });
}

export async function getAllCompaniesV2() {
  return await request({
    url: API_BASE_URL + "/companies/v2",
    method: "GET"
  });
}

export async function getAllCompaniesFilteredAndSorted(pageNo, elementsPerPage, filter, sort) {
  // the page number is displayed starting with 1 not 0
  var page = pageNo ? "?page="+ (pageNo - 1) : ""; 
  var view = elementsPerPage ? "&view=" + elementsPerPage : "";
  var sortParameter = "&sort=" + (sort === null ? "DATE_POSTED_DESCENDING" : sort);
  var filterCategoryIdsParameter = filter.categories.length > 0 ? "&categoryIds=" + filter.categories : "";
  let filterAddressFiltersParameter = "";
  if (filter.addresses.length > 0) {
    const addressFilters = JSON.stringify(filter.addresses);
    filterAddressFiltersParameter = `&addressFilters=${encodeURIComponent(addressFilters)}`;
  }
  var filterRatingAveragesParameter = filter.ratings.length > 0 ? "&ratingAverages=" + filter.ratings : "";
  // console.log("filterAddressFiltersParameter: ", filterAddressFiltersParameter);
  return await request({
    url: API_BASE_URL + "/companies" + page 
      + view
      + filterCategoryIdsParameter
      + filterAddressFiltersParameter
      + filterRatingAveragesParameter
      + sortParameter,
    method: "GET"
  });
}
// ----------------------------------------------------------Client
export async function postClient(client) {
  var newClientRequest = {
    ...client,
    provider: getProviderFromEmail(client.email)
  };
  // return await request({
  //   url: API_BASE_URL + "/clients",
  //   method: "POST",
  //   body: JSON.stringify(newClientRequest)
  // }).catch(error => console.log(error));;
  const headers = new Headers({ "Content-Type": "application/json" });

  return await fetch(API_BASE_URL + "/clients",  {
    method: "POST",
    headers: headers,
    body: JSON.stringify(newClientRequest)
  });
}

export async function putClient(client, profilePicture) {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    // console.log("putClient No access token set for getting the current user.");
    return Promise.reject("No access token set for getting the current user.");
  }
  const formData = new FormData();
  formData.append('id', client.id);
  formData.append('firstName', client.user.firstName);
  formData.append('lastName', client.user.lastName);
  formData.append('phone', client.user.phone);
  if (profilePicture) {
    formData.append(`profilePicture`, profilePicture);
  }
  return await request({
    url: API_BASE_URL + "/clients",
    isFormData: true,
    method: "PUT",
    body: formData
  });
}
// ----------------------------------------------------------Invitation
export async function activateAccount(invitationToken) {
  console.log("invitationToken", invitationToken);
  const headers = new Headers({ "Content-Type": "application/json" });

  return await fetch(API_BASE_URL + "/invitations/activate?invitationToken=" + invitationToken,  {
    method: "GET",
    headers: headers
  });
  // return await request({
  //   url: API_BASE_URL + "/invitations/activate?invitationToken=" + invitationToken,
  //   method: "GET"
  // });
}

// ----------------------------------------------------------Category
export function getCategoryById(id) {
  return request({
    url: API_BASE_URL + "/categories/" + id,
    method: "GET"
  });
}

export async function getAllCategories() {
  return await request({
    url: API_BASE_URL + "/categories",
    method: "GET"
  });
}

// ----------------------------------------------------------Search
export function searchAllBy(text) {
  return request({
    url: API_BASE_URL + "/search?text=" + text,
    method: "GET"
  });
}


// ----------------------------------------------------------Post
export async function postPost(post) {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    // console.log("postPost No access token set for getting the current user.");
    return Promise.reject("No access token set for getting the current user.");
  }
  const formData = new FormData();
  formData.append('companyId', post.companyId);
  formData.append('description', post.description);
  post.categoryIds.forEach((categoryId, index) => {
    formData.append(`categoryIds[${index}]`, categoryId);
  });
  formData.append('taggedClientId', 0);
  post.pictures.forEach((picture, index) => {
    formData.append(`pictures[${index}]`, picture);
  });

  return await request({
    url: API_BASE_URL + "/posts",
    isFormData: true,
    method: "POST",
    body: formData
  });
}

export async function getPostById(postId) {
  return await request({
    url: API_BASE_URL + "/posts/" + postId,
    method: "GET"
  });
}

export async function getAllPostsFilteredAndSorted(pageNo, elementsPerPage, filter, sort) {
  var page = pageNo - 1; //because we display the page numbers starting with 1 not 0
  var sortParameter = "&sort=" + (sort === null ? "DATE_POSTED_DESCENDING" : sort);
  var filterDateIntervalFromParameter = filter.dateInterval && filter.dateInterval.from ? "&dateFrom=" + filter.dateInterval.from : "";
  var filterDateIntervalToParameter = filter.dateInterval && filter.dateInterval.to ? "&dateTo=" + filter.dateInterval.to : "";
  var filterCategoryIdsParameter = filter.categories.length > 0 ? "&categoryIds=" + filter.categories : "";
  var filterCompanyIdsParameter = filter.companies.length > 0 ? "&companyIds=" + filter.companies : "";
  let filterAddressFiltersParameter = "";
  if (filter.addresses.length > 0) {
    const addressFilters = JSON.stringify(filter.addresses);
    filterAddressFiltersParameter = `&addressFilters=${encodeURIComponent(addressFilters)}`;
  }
  var filterRatingAveragesParameter = filter.ratings.length > 0 ? "&ratingAverages=" + filter.ratings : "";
  console.log("filterAddressFiltersParameter: ", filterAddressFiltersParameter);
  return await request({
    url: API_BASE_URL + "/posts?page=" + page
      + "&view=" + elementsPerPage
      + filterDateIntervalFromParameter
      + filterDateIntervalToParameter
      + filterCategoryIdsParameter
      + filterCompanyIdsParameter
      + filterAddressFiltersParameter
      + filterRatingAveragesParameter
      + sortParameter,
    method: "GET"
  });
}

export async function getAllPostsByCompanyId(pageNo, elementsPerPage, companyId) {
  var page = pageNo - 1; //because we display the page numbers starting with 1 not 0
  return await request({
    url: API_BASE_URL + "/posts/company/"+companyId+"?page=" + page
      + "&view=" + elementsPerPage,
    method: "GET"
  });
}

// ----------------------------------------------------------Message
export async function postMessage(message) {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    // console.log("postMessage No access token set for getting the current user.");
    return Promise.reject("No access token set for getting the current user.");
  }

  return await request({
    url: API_BASE_URL + "/messages",
    method: "POST",
    body: JSON.stringify(message)
  }).catch(error => console.log(error));;
}
