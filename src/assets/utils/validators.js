import phoneNumberUtil from 'google-libphonenumber/dist/libphonenumber';

const phoneUtil = phoneNumberUtil.PhoneNumberUtil.getInstance();
const REVERSED_TEST_KEY = [2, 3, 5, 7, 1, 2, 3, 5, 7];
export const registrationNumberRegex = /^(J|F|C)((0[1-9])|([1-3][0-9])|(40|51|52))\/([1-9][0-9]{0,7})\/[1-9][0-9]{3}$/;
export const taxIdRegex = /^(RO)?[1-9]\d{1,9}$/;
export const nameRegex = /^[a-zA-Z0-9ăîâșțĂÎÂȘȚ.-\s]{2,}$/;
export const emailRegex = /^[a-zA-Z0-9._%+-]+(@gmail|@yahoo)\.com$/;

function showTooltipOfInput(input, message) {
    const tooltip = input.nextSibling;
    input.classList.add("invalid");
    tooltip.style.display = "flex";
    tooltip.children[1].textContent = message;
}
function hideTooltipOfInput(input) {
    const tooltip = input.nextSibling;
    tooltip.style.display = "none";
    input.classList.remove("invalid");
}

export function validateRegistrationNumberEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateRegistrationNumber(value);
    !isValid ? showTooltipOfInput(input, "Numărul de ordine introdus nu este valid!") : hideTooltipOfInput(input);
}
export function validateRegistrationNumber(value) {
    return registrationNumberRegex.test(value);
}

export function validateTaxIdEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    var isValid = validateTaxId(value);
    !isValid ? showTooltipOfInput(input, "CIF-ul introdus nu este valid!") : hideTooltipOfInput(input);
}

export function validateTaxId(value) {
    var isValid = taxIdRegex.test(value);
    if (isValid) {
        let controlChar = value.includes("RO") ? value.charAt(11) : value.charAt(9);
        let code = value.includes("RO") ? value.substring(2, 11) : value.substring(0, 9);
        let sum = 0;
        for (let i = 0; i < 9; i++) {
            sum += REVERSED_TEST_KEY[i] * parseInt(code.substring(9 - 1 - i, 9 - i));
        }
        var verificationChar = String(sum * 10 % 11 % 10).charAt(0);
        isValid = verificationChar === controlChar ? true : false;
    }
    return isValid;
}

export function validateCompanyDescriptionEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateCompanyDescription(value);
    !isValid ? showTooltipOfInput(input, "Descrierea nu poate lipsi sau depăși 900 de caractere!")
        : hideTooltipOfInput(input);
}
export function validateCompanyDescription(value) {
    return value !== "" && value.length <= 900;
}


export function validateNameEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateName(value);
    !isValid ? showTooltipOfInput(input, "Numele introdus nu este valid! Acesta poate conține doar litere, cifre sau '.'!")
        : hideTooltipOfInput(input);
}
export function validateName(value) {
    return nameRegex.test(value);
}

export function validatePhoneEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validatePhone(value);
    !isValid ? showTooltipOfInput(input, "Numărul de telefon nu este valid! Asigurați-vă că acesta conține prefixul \"+40\"!")
        : hideTooltipOfInput(input);
};
export function validatePhone(value) {
    let isValid = false;
    try {
        isValid = phoneUtil.isValidNumber(phoneUtil.parse(value));
    } catch (e) {
        isValid = false;
    }
    return isValid;
}
export function validateEmailEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateEmail(value);
    !isValid ? showTooltipOfInput(input, "Adresa de e-mail introdusă nu este validă sau nu este suportată! Momentan sunt acceptate doar adrese Gmail sau Yahoo!")
    : hideTooltipOfInput(input);
}
export function validateEmail(value) {
    return emailRegex.test(value);
}

export function validateStreetEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateStreet(value);
    !isValid ? showTooltipOfInput(input, "Numele străzii introduse nu este valid!")
    : hideTooltipOfInput(input);
}
export function validateStreet(value) {
    return value.length >= 1;
}
export function validateNumberEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validateNumber(value);
    !isValid ? showTooltipOfInput(input, "Numărul introdus nu este valid!")
    : hideTooltipOfInput(input);
}
export function validateNumber(value) {
    return value.length >= 1;
}
export function validatePostDescriptionEvent(event) {
    const input = event.target;
    const value = input.value.trim();
    const isValid = validatePostDescription(value);
    !isValid ? showTooltipOfInput(input, "Descrierea nu poate lipsi sau depăși 3000 de caractere!")
    : hideTooltipOfInput(input);
}
export function validatePostDescription(value) {
    return value !== "" && value.length <= 3000;
}