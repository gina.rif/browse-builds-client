import { GOOGLE_MAP_API_KEY as GOOGLE_MAP_API_KEY } from "../../constants";
import { useUserStore } from "../../stores/user";
export async function geocodeAddress(county, city, street, number) {
    try {
        const response = await fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" +
            number + " " + street + " " + city + " " + county + " Romania" +
            "&key=" + GOOGLE_MAP_API_KEY, {
            method: "GET",
        }).then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            }));
        const r = response.results[0];
        const location = r.geometry.location;
        return location;
    } catch (error) {
        console.error('Geocoding failed:', error);
        return;
    }
};

export function getGLocation(street, number, city, county) {
    return 'https://www.google.com/maps/place/' + street + ' '
        + number + ' '
        + city + ' '
        + county;
}
export function getMailLink(email) {
    const userStore = useUserStore();
    if (userStore.authenticated && userStore.currentUser.provider === "google") {
        return 'https://mail.google.com/mail/?view=cm&fs=1&to=' + email;
    } else if (userStore.authenticated && userStore.currentUser.provider === "yahoo") {
        return "https://compose.mail.yahoo.com/?to=" + email;
    } else return "mailto:" + email;
}
