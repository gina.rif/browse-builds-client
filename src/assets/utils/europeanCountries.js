export const europeanCountries = [
    {
        id: "RO",
        name: "România"
    }
    ,{
        id: "AX",
        name: "Insulele Aland"
    },
    {
        id: "AL",
        name: "Albania"
    },
    {
        id: "AD",
        name: "Andorra"
    },
    {
        id: "AT",
        name: "Austria"
    },
    {
        id: "BY",
        name: "Belarus"
    },
    {
        id: "BE",
        name: "Belgia"
    }
    ,
    {
        id: "BA",
        name: "Bosnia și Hertegovina"
    },
    {
        id: "BG",
        name: "Bulgaria"
    },
    {
        id: "HR",
        name: "Croaţia"
    },
    {
        id: "CZ",
        name: "Republica Cehă"
    },
    {
        id: "DK",
        name: "Danemarca"
    },
    {
        id: "EE",
        name: "Estonia"
    },
    {
        id: "FO",
        name: "Insulele Feroe"
    },
    {
        id: "FI",
        name: "Finlanda"
    },
    {
        id: "FR",
        name: "Franţa"
    },
    {
        id: "DE",
        name: "Germania"
    },
    {
        id: "GI",
        name: "Gibraltar"
    },
    {
        id: "GR",
        name: "Grecia"
    },
    {
        id: "VA",
        name: "Sfântul Scaun (Statul Vatican)"
    },
    {
        id: "HU",
        name: "Ungaria"
    },
    {
        id: "IS",
        name: "Islanda"
    },
    {
        id: "IE",
        name: "Irlanda"
    },
    {
        id: "IM",
        name: "Insula Barbatului"
    },
    {
        id: "IT",
        name: "Italia"
    },
    {
        id: "JE",
        name: "Jersey"
    },
    {
        id: "XK",
        name: "Kosovo"
    },
    {
        id: "LV",
        name: "Letonia"
    },
    {
        id: "LI",
        name: "Liechtenstein"
    },
    {
        id: "LT",
        name: "Lituania"
    },
    {
        id: "LU",
        name: "Luxemburg"
    },
    {
        id: "MK",
        name: "Macedonia, Fosta Republică Iugoslavă a"
    },
    {
        id: "MT",
        name: "Malta"
    },
    {
        id: "MD",
        name: "Moldova, Republica"
    },
    {
        id: "MC",
        name: "Monaco"
    },
    {
        id: "ME",
        name: "Muntenegru"
    },
    {
        id: "NL",
        name: "Olanda"
    },
    {
        id: "NO",
        name: "Norvegia"
    },
    {
        id: "PL",
        name: "Polonia"
    },
    {
        id: "PT",
        name: "Portugalia"
    },
    {
        id: "RO",
        name: "România"
    },
    {
        id: "SM",
        name: "San Marino"
    },
    {
        id: "RS",
        name: "Serbia"
    },
    {
        id: "CS",
        name: "Serbia și Muntenegru"
    },
    {
        id: "SK",
        name: "Slovacia"
    },
    {
        id: "SI",
        name: "Slovenia"
    },
    {
        id: "ES",
        name: "Spania"
    },
    {
        id: "SJ",
        name: "Svalbard și Jan Mayen"
    },
    {
        id: "SE",
        name: "Suedia"
    },
    {
        id: "CH",
        name: "Elveția"
    },
    {
        id: "UA",
        name: "Ucraina"
    },
    {
        id: "GB",
        name: "Regatul Unit"
    }
];