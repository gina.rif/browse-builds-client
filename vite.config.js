import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
// import { redirectUri, apiBaseUrl } from "./src/config/keys";


export default defineConfig({
  // define: {
  //   'process.env': {},
  // },
  plugins: [vue(), vueJsx()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  server: {
    mimeTypes: {
      'application/javascript': ['vue']
    },
    proxy: {
      '/api': {
        // target: "http://localhost:8080", 
        target: process.env.VITE_API_BASE_URL,
        changeOrigin: true,
        ws: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
        secure: false
      }
    },
    charset: 'utf-8',
  },
  devServer: {
    mimeTypes: {
      'application/javascript': ['vue']
    },
    proxy: {
      '/api': {
        target: "http://localhost:8080",
        changeOrigin: true,
        ws: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
        secure: false
      }
    }
  },
  build: {
    rollupOptions: {
      external: ['google-libphonenumber'],
    },
  },
});
