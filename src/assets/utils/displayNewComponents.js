import FullPicture from "../../components/FullPicture.vue";
import NewMessage from "../../components/NewMessage.vue";
import { defineComponent, createApp, h} from 'vue';

export function displayFullPicture( isFromLocale, file) {
    const fullPictureComponent = defineComponent({
        components: { FullPicture },
        render() {
            return h(FullPicture, {
                id: isFromLocale ? file.file.name : file.id,
                url: file.url
            });
        }
    });
    const appFullPicture = createApp(fullPictureComponent);
    appFullPicture.mount("#full-picture");
    document.getElementById("full-picture").style.display="contents";
};

export function displayNewMessage( toUser) {
    const newMessageComponent = defineComponent({
        components: { NewMessage },
        render() {
            return h(NewMessage, {
                toUser: toUser,
            });
        }
    });
    const appNewMessage = createApp(newMessageComponent);
    appNewMessage.mount("#new-message");
    document.getElementById("new-message").style.display="contents";
};