import { createApp, watch, ref } from "vue";
import { createPinia } from "pinia";

import "./assets/main.css";

import App from './App.vue';
import router from './router';
import vue3GoogleLogin from 'vue3-google-login';

export const isComponentMounted = ref(false);

const app = createApp(App)

app.use(vue3GoogleLogin, {
    clientId: import.meta.env.VITE_GOOGLE_OAUTH_CLIENT_ID
});

const pinia = createPinia();

watch(pinia.state, (state) => {
    localStorage.setItem("user", JSON.stringify(state.user));
},
    { deep: true }
);



router.beforeResolve((to, from, next) => {
    if (!isComponentMounted.value){ //||  from.path !== to.path) {
        // Delay the navigation, using a timer
        console.log("delaying navigation to "+ to.path+" from "+from.path);
        setTimeout(() => { next(); }, 5000);
    } else {
        next(); 
    }
});

router.isReady().then(() => {
    isComponentMounted.value = true;
});

app.use(pinia)
    .use(router)
    .mount('#app');


