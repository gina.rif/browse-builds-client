import { romanianCountiesWithCities } from "./romanianCountiesWithCities";

export function setCitiesListForCounty(county) {
    var currentCounty = romanianCountiesWithCities.filter(c => c.county == county);
    var cities;
    if (currentCounty.length != 0) {
        let uniqueSet= new Set(currentCounty[0].cities);
        cities = Array.from(uniqueSet).sort();
    }
    else cities = "";
    return cities;
}