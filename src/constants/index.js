export const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;
export const ACCESS_TOKEN = "";
export const CLIENT_URL = import.meta.env.VITE_CLIENT_URL;
export const OAUTH2_REDIRECT_URI = CLIENT_URL+"/oauth2/redirect";
export const GOOGLE_AUTH_URL = API_BASE_URL + "/oauth2/authorize/google?redirect_uri=" + OAUTH2_REDIRECT_URI;
export const YAHOO_AUTH_URL = API_BASE_URL + "/oauth2/authorize/yahoo?redirect_uri=" + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = API_BASE_URL + "/oauth2/authorize/facebook?redirect_uri=" + OAUTH2_REDIRECT_URI;
export const GOOGLE_MAP_API_KEY = import.meta.env.VITE_GOOGLE_MAP_API_KEY;
