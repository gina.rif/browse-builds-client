import { useUserStore } from "../../stores/user";
import { ACCESS_TOKEN } from "../../constants";
import { getCurrentUser } from "./APIUtils";
import { useRouter, useRoute } from "vue-router";




export async function OAuthRedirectHandler(token) {
    const userStore=useUserStore();
    const router = useRouter();
    console.log("OAuthRedirectHandler Token: ", token);
    if (token) {
        localStorage.setItem(ACCESS_TOKEN, token);
        console.log("OAuthRedirectHandler set token");

        
        userStore.setLoading(true);
    await getCurrentUser()
        .then(response => {
            if (response.emailVerified) {
                console.log("OAuthRedirectView Logged in user: ", response);
                userStore.login();
                userStore.$patch({ currentUser: response });
            }
        }).catch(error => {
            console.log("OAuthRedirectView error: ", error);
        })
        userStore.setLoading(false);


        if (userStore.authenticated && userStore.currentUser.emailVerified) {
            console.log("OAuthRedirectHandler user is authenticated. Redirecting to profile...");

            router.push({ name: 'profile', params: { id: userStore.currentUser.user.id } });
        } else {
            alert("Adresa de e-mail cu care ați încercat să vă autentificați nu a fost validată! Verificați mesajele primite pe e-mail pentru a o valida!");
            router.push("/login");
        }
    } else {
        console.log("OAuthRedirectHandler logging out");

        userStore.logout();
        router.push("/login");
    }
}