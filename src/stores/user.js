import { defineStore } from "pinia";
import { ACCESS_TOKEN } from "../constants";
import { useRouter } from "vue-router";


export const useUserStore = defineStore("user", {
    state: () => {
        if (localStorage.getItem("user")) {
            console.log("User is loggedin");
            return JSON.parse(localStorage.getItem("user"));
        }
            console.log("User is not loggedin");
        return {
            authenticated: false,
            loading: false,
            currentUser: null
        };
    },
    actions: {
        login() {
            this.authenticated = true;
        },
        logout() {
            const router = useRouter();
            this.authenticated = false;
            this.currentUser = null;
            localStorage.setItem(ACCESS_TOKEN, "");
            console.log("userStore logging out ")
            // router.push("/login");
        },
        setLoading(bool) {
            this.loading = bool;
        },
    },
})